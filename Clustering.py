import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import pandas as pd
import os
import numpy as np
from sklearn.cluster import KMeans
import plotly.express as px

plt.style.use('ggplot')

inputs1 = "../Data/part1.csv"
outputs = "../Results/"

MyDataFrame1 = pd.read_csv(os.path.abspath(inputs1), sep=",")
df = MyDataFrame1[["FIRST_COURSE", "SECOND_COURSE", "THIRD_COURSE"]]
print(MyDataFrame1)
print(df)

kmeans = KMeans(
    init="random",
    n_clusters=4,
    n_init=10,
    max_iter=300,
    random_state=42
)

kmeans.fit(df)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

# setting colors and scatter plotting the results

colormap = np.array(['Red', 'green', 'blue', "yellow"])
ax.scatter(df.FIRST_COURSE, df.SECOND_COURSE, df.THIRD_COURSE, c=colormap[kmeans.labels_])
plt.suptitle('Kmeans clustering\n'
             'according to the graph, we can state below groups of customers:\n'
             'Red: Business (cluster 1),\n'
             'Green: Retirement (cluster 2), \n '
             'Blue: Healthy (cluster 3),\n'
             'Yellow: Normal (cluster 4)'
             )

ax.set_xlabel("FIRST_COURSE")
ax.set_ylabel("SECOND_COURSE")
ax.set_zlabel("THIRD_COURSE")

plt.savefig(os.path.abspath('../Results/KmeansScatterPlt.png'))

#Adding labels to the dataset

predict = kmeans.predict(df)
MyDataFrame1['clusters'] = pd.Series(predict, index=df.index)
MyDataFrame1.to_csv(os.path.abspath('../Results/LabelsToCsv.csv'), sep=',')

# Grouping by the new column variables to have an overview
# Checking for allocation in each cluster
df = MyDataFrame1.groupby(['clusters']).count()
print("________________________________________________________________________________________________\n"
      "This is the allocation between groups\n"
      "________________________________________________________________________________________________\n",df)


print("________________________________________________________________________________________________\n"
      "This is the output of the original dataset added with the Labels\n"
      "________________________________________________________________________________________________\n",
      MyDataFrame1)

# Lets substract costumers per cluster in case we want to use it to propose a promotion
BusinessGroup = MyDataFrame1[MyDataFrame1["clusters"] == 0]
RetirementGroup = MyDataFrame1[MyDataFrame1["clusters"] == 1]
HealthyGroup = MyDataFrame1[MyDataFrame1["clusters"] == 2]
NormalGroup = MyDataFrame1[MyDataFrame1["clusters"] == 3]

print("________________________________________________________________________________________________\n"
      "This is an example of Retirement costumers group \n"
      "________________________________________________________________________________________________\n",
      RetirementGroup)
